﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    private float _speed = 10;
    [SerializeField]
    private float _jumpForce = 2;
    [SerializeField]
    private float _gravity = 10;
    [SerializeField]
    private Transform _sight;
    //TODO: Merge gravity with others
    private Rigidbody _rb;

    private bool _grounded = true;
	// Use this for initialization
	private void Start () {
        _rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	private void FixedUpdate () {
        if (_grounded)
        {
            Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            targetVelocity = transform.TransformDirection(targetVelocity);
            targetVelocity *= _speed;

            Vector3 velocity = _rb.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.y = 0;
            _rb.AddForce(velocityChange, ForceMode.VelocityChange);

            if(Input.GetButton("Jump"))
            {
                _rb.velocity = new Vector3(_rb.velocity.x, CalculateJump(), _rb.velocity.z);
            }
        }

        _rb.AddForce(new Vector3(0, (-1) * _gravity * _rb.mass, 0));
        transform.rotation = _sight.rotation;
        _grounded = false;
    }

    private void OnCollisionStay(Collision collision)
    {
        _grounded = true;
    }

    private float CalculateJump()
    {
        return Mathf.Sqrt(2 * _jumpForce * _gravity);
    }
}

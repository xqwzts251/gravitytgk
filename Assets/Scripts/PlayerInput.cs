﻿using System;
using UnityEngine;

/// <summary>
/// Handles player input
/// </summary>
[Serializable]
class PlayerInput : MonoBehaviour
{
    #region Inspector Variables
    [Header("Input")]
    [SerializeField] private KeyCode _RunKey     = KeyCode.LeftShift;
    [SerializeField] private KeyCode _JumpKey    = KeyCode.Space;
    [SerializeField] private KeyCode _PutFlagKey = KeyCode.E;

    [Header("Mouse")]
    [SerializeField] private float _SensivityHorizontal;
    [SerializeField] private float _SensivityVertical;
    #endregion Inspector Variables

    #region Public Variables
    public float Forward
    {
        get { return _VerticalAxis; }
    }

    public float Horizontal
    {
        get { return _MouseHorizontal * _SensivityHorizontal; }
    }
    public float Vertical
    {
        get { return _MouseVertical * _SensivityVertical; }
    }

    public bool Run   { get { return _Run;   } }
    public bool Jump  { get { return _Jump;  } }
    public bool Flag  { get { return _Flag;  } }
    public bool Shoot { get { return _Shoot; } }
    #endregion Public Variables

    #region Private Variables
    private float _VerticalAxis;

    private float _MouseVertical;
    private float _MouseHorizontal;

    private bool _Jump;
    private bool _Run;
    private bool _Flag;
    private bool _Shoot;
    #endregion Private Variables

    #region Unity Messages
    private void Update()
    {
        _VerticalAxis = Input.GetAxis("Vertical");

        _MouseHorizontal = Input.GetAxis("Mouse X");
        _MouseVertical   = Input.GetAxis("Mouse Y");

        _Run  = Input.GetKey(_RunKey);
        _Jump = Input.GetKeyDown(_JumpKey);

        _Flag  = Input.GetKey(_PutFlagKey);
        _Shoot = Input.GetMouseButtonDown(0);
    }
    #endregion Unity Messages
}

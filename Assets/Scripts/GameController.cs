﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	private GameObject enemy;

	[SerializeField]
	private Vector2 _spawnRange = new Vector2(-100, 100);

	[SerializeField]
	float _spawnInterval = 10.0f;

	void Start () {
		InvokeRepeating("SpawnEnemy", 3, _spawnInterval);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy() {
		float posX = Random.Range(_spawnRange[0], _spawnRange[1]);
		float posZ = Random.Range(_spawnRange[0], _spawnRange[1]);

		Instantiate(enemy, new Vector3(posX, 1.5f, posZ), Quaternion.identity);
	}
}

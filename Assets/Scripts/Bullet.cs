﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {

    public float SpawnSpeed { get { return _spawnSpeed; } }
    public Vector3 Gravity { get; set; }

    [SerializeField]
    private float _spawnSpeed = 100;
    [SerializeField]
    private float _deadTime = 5f;
    private Rigidbody _rb;

    // Use this for initialization
    private void Start () {
        _rb = GetComponent<Rigidbody>();
        _rb.useGravity = false;
        StartCoroutine(Dead());
    }

    private IEnumerator Dead()
    {
        yield return new WaitForSeconds(_deadTime);
        Destroy(gameObject);
    }

	private void OnTriggerEnter(Collider other) {
		if(other.tag == "Enemy") {
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

	#region Serialized
	[SerializeField] GameObject _top;
	[SerializeField]
	float _speed = 1;

	#endregion

	#region Privates
	private float _bottomPosition;
	private float _direction;
	#endregion

	// Use this for initialization
	void Start () {
		//assume always starts from bottom
		_direction = 1.0f;
		_bottomPosition = transform.position.y;
	
	}

	// Update is called once per frame
	void Update () {
		Move();
	}


	private void Move() {
		transform.Translate(Vector3.up * _direction * _speed * Time.deltaTime, Space.World);
		if (transform.position.y >= _top.transform.position.y) {
			_direction = -1.0f;
		}
		else if (transform.position.y <= _bottomPosition){
			_direction = 1.0f;
		}
	}
}

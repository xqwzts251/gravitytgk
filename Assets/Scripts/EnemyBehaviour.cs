﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {

	private NavMeshAgent _nav;
	private Transform _target;

	[SerializeField]
	private float _shootingInterval;

	EnemyWeapon _weapon;
	
	// Use this for initialization
	void Start () {
		_target = GameObject.FindGameObjectWithTag("Player").transform;
		_nav = GetComponent<NavMeshAgent>();
		_weapon = GetComponentInChildren<EnemyWeapon>();
		InvokeRepeating("Shoot", _shootingInterval, _shootingInterval);
	}
	
	// Update is called once per frame
	void Update () {
		_nav.SetDestination(_target.position);
	}

	void Shoot() {
		_weapon.Shoot();
	}
}

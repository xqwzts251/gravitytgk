﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Vector3 Gravity { set; get; }
    #region Serialized values
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _jump_speed;
    #endregion Serialized values

    private Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
    
    private void FixedUpdate() {
        float move_horizontal = Input.GetAxis("Horizontal");
        float move_vertical = Input.GetAxis("Vertical");

        rb.AddForce(new Vector3(move_horizontal, 0, move_vertical) * _speed);

        if (Input.GetKeyUp(KeyCode.Space)) {
            Jump();
        } 
    }

    private void Jump() {
        rb.AddForce(Gravity * _jump_speed);
    }
}

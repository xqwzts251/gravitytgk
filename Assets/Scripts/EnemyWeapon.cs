﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour {

	[SerializeField]
	private Transform _sight;
	[SerializeField]
	private Bullet _bullet;
	// Use this for initialization
	public void Shoot() {
		var newBullet = Instantiate(_bullet, _sight.position, Quaternion.identity);
		Vector3 force = (_sight.position - transform.position) * newBullet.SpawnSpeed;
		newBullet.GetComponent<Rigidbody>().AddForce(force);
	}
}

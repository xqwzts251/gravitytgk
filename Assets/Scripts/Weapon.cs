﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	[SerializeField]
	private Transform _sight;
	[SerializeField]
	private Bullet _bullet;

	private void Update() {
		if (Input.GetButtonDown("Fire1")) {
			Shoot();
		}
	}

	public void Shoot() {
		var newBullet = Instantiate(_bullet, _sight.position, Quaternion.identity);
		Vector3 force = (_sight.position - transform.position) * newBullet.SpawnSpeed;
		newBullet.GetComponent<Rigidbody>().AddForce(force);
	}
}

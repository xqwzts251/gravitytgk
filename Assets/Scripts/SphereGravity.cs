﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGravity : MonoBehaviour {
    #region Inspector variables
    [SerializeField]
    [Range(-30, 30)]
    private float forcePower = 9.88f;

    #endregion Inspector variables
    #region Private Variables
    private SphereCollider gravityCollider;
    private List<Rigidbody> targets = new List<Rigidbody>();
    #endregion Private Variables

    // Update is called once per frame
    void FixedUpdate () {
        foreach(var target in targets) {
            if(target != null)
                AttractObject(target);
        }
        targets.RemoveAll(item => item == null);
    }

    private void OnTriggerExit(Collider other) {
        DismissObject(other);
    }

    private void DismissObject(Collider other) {
        var exiting = other.gameObject.GetComponent<Rigidbody>();
        if (exiting == null) return;
        if(other.tag != "Bullet" ) {
            exiting.useGravity = true;
        }
        targets.Remove(exiting);
    }

    private void OnTriggerEnter(Collider other) {
        InitAttraction(other);
    }

    private void InitAttraction(Collider other) {
        var entering = other.gameObject.GetComponent<Rigidbody>();
        if (entering == null) return;
        entering.useGravity = false;
        targets.Add(entering);
    }

    private void AttractObject(Rigidbody targetRb) {
        var forceVector = (transform.position - targetRb.position).normalized * forcePower;
        var distance = (transform.position - targetRb.position).magnitude;
        var attractionForce = forceVector / distance / targetRb.mass;
        targetRb.AddForce(attractionForce, ForceMode.Acceleration);
    }
}

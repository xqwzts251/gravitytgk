﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    [SerializeField]
    private float _sensitivity = 1;
    [SerializeField]
    private float _xRotLimit = 60;

    private float _xRot = 0;
    private float _yRot = 0;

    
    // Update is called once per frame
    void Update () {
        _yRot += _sensitivity * Input.GetAxis("Mouse X");
        _xRot += _sensitivity * (-1) * Input.GetAxis("Mouse Y");

        _xRot = Mathf.Clamp(_xRot, -_xRotLimit, _xRotLimit);

        transform.rotation = Quaternion.Euler(_xRot, _yRot, 0);
    }
}
